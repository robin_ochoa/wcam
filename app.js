var express = require("express");
var app = new express();
var http = require("http").Server(app);
var io = require("socket.io")(http);
var getUserMedia = require('getusermedia');

var Log = require('log'),
	log = new Log('info');

var port = process.env.PORT || 3000;

app.use(express.static(__dirname + "/public"));

app.get('/', function(req,res){
	res.redirect('index.html');
});

//Escuchar conexion de un usuario
io.on('connection',function(socket){
	//Se envian imagenes por segundo
	socket.on('stream',function(image){
		socket.broadcast.emit('stream',image);
	});
});

http.listen(port,function(){
	log.info('Servidor escuchando a traves del puerto %s', port);
});

(function(){ // Your closure
    var ConsoleLogHTML = require('console-log-html');
})();

/*
getUserMedia(function (err, stream) {
    // if the browser doesn't support user media
    // or the user says "no" the error gets passed
    // as the first argument.
    if (err) {
       console.log('failed');
    } else {
       console.log('got a stream', stream);  
    }
});
*/